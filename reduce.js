function reduce(elements,cb,startingValue){
    if(startingValue==undefined){
        startingValue=elements[0];
    }
    for(i=0;i<elements.length;i++){
        startingValue=cb(startingValue,elements[i]);
    }
    return startingValue;    
}
module.exports={reduce};