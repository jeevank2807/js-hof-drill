function filter(elements,cb){
    let truth;
    const arr=[];
    for(i=0;i<elements.length;i++){
        truth=cb(elements[i]);
        if(truth==true){
            arr.push(elements[i]);
        }
    }
    return arr;
}
module.exports ={filter};