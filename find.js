function find(elements,cb){
    let result,index,truth;
    for(i=0;i<elements.length;i++){
        truth=cb(elements[i]);
        if(truth==true){
            result=truth;
            index=i;
            break;
        }
    }
    if(result==false){
        return 'undefined';
    }
    return elements[index];
}
module.exports ={find};